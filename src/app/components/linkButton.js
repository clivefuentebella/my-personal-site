import Link from "next/link";

export default function LinkButton({ additionalClasses, href, children }) {
  return (
    <Link
      className={"rounded py-2 px-3 " + additionalClasses}
      href={href}
      target={href == "#" ? "" : "_blank"}
    >
      {children}
    </Link>
  );
}
