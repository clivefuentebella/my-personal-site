import LinkButton from "@/app/components/linkButton";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faAt,
  faPhone,
  faLocationPin,
  faStarOfLife,
} from "@fortawesome/free-solid-svg-icons";
import { faLinkedin } from "@fortawesome/free-brands-svg-icons";

import { poppins } from "./fonts.js";

export default function Home() {
  return (
    <>
      <main className="flex h-screen">
        <div className="m-auto text-center">
          <h1 className={poppins.className + " text-5xl mb-3"}>
            Hi, I am Clive!
          </h1>
          <div className="grid grid-cols-1 md:grid-cols-3 mb-8">
            <div>
              <a
                href="mailto:clivefuentebella@gmail.com"
                className="hover:underline"
              >
                <FontAwesomeIcon icon={faAt} /> clivefuentebella@gmail.com
              </a>
            </div>
            <div>
              <FontAwesomeIcon icon={faPhone} /> +63 999 2016 212
            </div>
            <div>
              <FontAwesomeIcon icon={faLocationPin} /> Antipolo, Rizal,
              Philippines
            </div>
          </div>
          <div className="mb-3">
            <LinkButton
              additionalClasses="bg-blue-500 hover:bg-blue-700 text-white mr-4"
              href="https://ph.linkedin.com/in/clivefuentebella"
            >
              <FontAwesomeIcon icon={faLinkedin} /> &nbsp;Linkedin
            </LinkButton>
            <LinkButton
              additionalClasses="bg-blue-500 hover:bg-blue-700 text-white mr-4"
              href="/resume.pdf"
            >
              <FontAwesomeIcon icon={faStarOfLife} /> &nbsp;Resume
            </LinkButton>
          </div>
        </div>
      </main>
    </>
  );
}
