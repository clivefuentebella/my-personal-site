import "./globals.css";
import "@fortawesome/fontawesome-svg-core/styles.css";
import { inter } from "./fonts.js";
import { config } from "@fortawesome/fontawesome-svg-core";
config.autoAddCss = false;

export const metadata = {
  title: "Clive Fuentebella",
  description: "Personal website for Clive Fuentebella",
};

export default function RootLayout({ children }) {
  return (
    <html lang="en">
      <body className={inter.className}>{children}</body>
    </html>
  );
}
